from influxdb_client import InfluxDBClient, Point
from influxdb_client.client.write_api import SYNCHRONOUS
import pathlib
import statistics
import csv

client = InfluxDBClient(url="http://10.5.4.20:30004/", token="basstoken", org="5gdive")

write_api = client.write_api(write_options=SYNCHRONOUS)
query_api = client.query_api()

q = '''
    from(bucket: "DEEP_Uptime")
  |> range(start: -40d)
  |> filter(fn: (r) => r["_measurement"] == "monitor_status")
  |> filter(fn: (r) => r["monitor_name"] == "IESS")
  |> filter(fn: (r) => r["_field"] == "gauge")
'''
p = '''
    from(bucket: "DEEP_Uptime")
  |> range(start: -40d)
  |> filter(fn: (r) => r["_measurement"] == "monitor_status")
  |> filter(fn: (r) => r["monitor_name"] == "BASS")
  |> filter(fn: (r) => r["_field"] == "gauge")
'''
## using Table structure
rows=0
sum=0
tables1= query_api.query(q)
for table in tables1:
    for row in table.records:
        sum=sum+row.values['_value']
        rows=rows+1

print(f'Availability(IESS): {sum/rows*100} %')


rows=0
sum=0
tables1= query_api.query(p)
for table in tables1:
    for row in table.records:
        sum=sum+row.values['_value']
        rows=rows+1

print(f'Availability(BASS): {sum/rows*100} %')


